<?php

/**
 * PC Shop common helper module
 */

// Works for 4.7 & 5.0
if (function_exists('theme_add_style')) {
  theme_add_style(drupal_get_path('module', 'erp') .'/erp.css');
}
else {
  drupal_add_css(drupal_get_path('module', 'erp') .'/erp.css');
}

function shrink($text, $shrink = 0) {
  if (strlen($text) > $shrink + 2) {
    return substr($text, 0, $shrink) .'..';
  }
  else {
    return ($text ? $text : '');
  }
}


/**
 * Validate a job/timesheet/whatever's owner
 *
 * @param unknown_type $node
 * @return unknown
 */
function erp_owner_validate($name) {
  // Try and retrieve the uid for the job owner based on the name
  $owner = user_load(array('name' => $name));

  return ($owner->uid ? $owner->uid : 0);
}

function erp_oid_validate($oid) {
  // Try and retrieve the owner name from the uid
  $owner = user_load(array('uid' => $oid));

  return ($owner->name ? $owner->name : '');
}

/**
 * Debugging function to list variables
 *
 * @param unknown_type $var
 * @return unknown
 */
function erp_var_list($var, $html = true) {
  $string = '';

  if (is_string($var)) {
    $string = $var;
  }
  elseif (is_array($var)) {
    foreach ($var as $key => $value) {
      $string .= ($html ? '<li>' : '') . $key . ' : ';

      if (is_array($value) or is_object($value)) {
        $string .= ($html ? '<ul>' : '') .
          erp_var_list($value, $html) .
          ($html ? '</ul></li>' : "\n");
      }
      else {
        $string .= $value . ($html ? '</li>' : "\n");
      }
    }
  }
  elseif (is_object($var)) {
    foreach (get_object_vars($var) as $key => $value) {
      $string .= ($html ? '<li>' : '') . $key . ($html ? ' : <ul>' : '');
      $string .= erp_var_list($value, $html) . ($html ? '</ul></li>' : '');
    }
  }
  else {
    $string .= "\n" . ($html ? '<BR>' : '');
  }
  return $string;
}

function erp_find_path_query($url) {
  global $base_url, $base_path;

  // TODO: This doesn't work properly unless using clean URL's
  $source = parse_url($url);
  $path = $source['path'];
  $new_base = substr(substr($base_path, 1), 0, -1);

  if ($base_path != "/") {
    $path = str_replace($base_path, '', $path);
  }

  $query = $source['query'];
  return array($path, $query);
}

/**
 * Function used to both display and set the next invoice number,
 * po number, etc. see invoice_settings, purchase_order_settings
 * Defaults to collapsed as its not common
 *
 * TODO: I dont think this code will work with postgresql as it uses "REPLACE".
 *
 * @param unknown_type $type
 * @param unknown_type $desc
 * @return unknown
 */
function erp_set_next_id_form($form, $type, $desc) {
  $op = $form_values['op'];

  if ($op == t('Set number')) {
    db_query("REPLACE INTO {sequences} (name, id) VALUES ('%s', %d)", $type, $_POST['edit'][$type .'_number']);
  }

  $result = db_query("SELECT id FROM {sequences} WHERE name = '%s'", $type);
  if (db_num_rows($result)) {
    $invoice_id = array_pop(db_fetch_array($result));
  }

  $form[$type .'_settings']['set_next'] =
    array('#type' => 'fieldset',
          '#title' => t('Next ') . $desc . t(' number'),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#description' => t('You can set the number for the next created ') . $desc . t(' here.'));

  $form[$type .'_settings']['set_next'][$type .'_number'] =
    array('#type' => 'textfield',
          '#size' => 10,
          '#maxlength' => 10,
          '#title' => t('Next ') . $desc . t(' number'),
          '#default_value' => $invoice_id);

  $form[$type .'_settings']['set_next'][$type .'_button'] =
    array('#type' => 'button',
          '#value' => t('Set number'));

  return $form;
}

/**
 * This is is common function used by quote, invoice, cash_sale, purchase_order, and so is
 * placed here. It does depend on "erp_item" being enabled however, meaning all those depend
 * on erp_item as well.
 *
 */
function erp_add_newline(&$node) {
  if ($_POST['addline'] == t('Add line')) {
    $newline = $_POST['newline'];

    if ($newline['item_desc']) {
      list($supplier_code, $stock_code, $desc, $price) = erp_item_desc_breakup($newline['item_desc']);
      $item_nid = erp_item_locate($supplier_code, $stock_code, FALSE);
      if ($item_nid) {
        $item_node = node_load($item_nid);

        erp_set_entries_form_value($node, 'item', $item_nid);
        erp_set_entries_form_value($node, 'item_desc', erp_item_desc_build($item_node));
        erp_set_entries_form_value($node, 'qty', $newline['qty']);
        erp_set_entries_form_value($node, 'completed_date', $newline['completed_date']);
        erp_set_entries_form_value($node, 'extra', $newline['extra']);
        erp_set_entries_form_value($node, 'serials', $newline['serials']);
        erp_set_entries_form_value($node, 'price', $newline['price']);
      }
      else {
        form_set_error('newline][item_desc', t('Invalid stock code'));
      }
    }
  }

  for ($i = 0; $i < count($_POST['entries']['qty']); $i++) {
    if ($_POST['delete-'. $i] == 'X') {
      foreach ($node->entries as $value => $key) {
        if (is_array($node->entries[$value])) {
          erp_delete_array_key($node->entries[$value], $i);
          erp_delete_array_key($_POST['entries'][$value], $i);
        }
      }
    }
  }
}

function erp_set_entries_form_value(&$node, $field, $value) {
  $x['#parents'] = array('entries', $field, $i, '#default_value');
  form_set_value($x, $value);
  unset($x);
  $node->entries[$field][] = $value;
}

/**
 * Given an array, remove one item, array_splice renumbers as well, which is nice.
 *
 * @param array $array
 * @param int $key
 */
function erp_delete_array_key(&$array, $key) {
  array_splice($array, $key, 1);
  $temp = $array;
}

/**
 * Helper function to display invoice entries.
 *
 * @param unknown_type $form
 * @return unknown
 */
function theme_entries($form) {
  $rows = array();

  $header = build_entries_header($form);

  foreach (element_children($form['qty']) as $key) {
    $row_data[] = drupal_render($form['line'][$key]) . drupal_render($form['qty'][$key]);
    $row_data[] = drupal_render($form['item_desc'][$key]) . drupal_render($form['item'][$key]); //hidden

    if (isset($form['completed_date'][0])) {
      $row_data[] = drupal_render($form['completed_date'][$key]);
    }

    if (isset($form['extra'][0])) {
      $row_data[] = drupal_render($form['extra'][$key]);
    }

    // Invoices etc.
    if (isset($form['serials'][0])) {
      $row_data[] = drupal_render($form['serials'][$key]);
    }

    // Goods receive has individual serials
    if (isset($form['serial'][0])) {
      $row_data[] = drupal_render($form['serial'][$key]);
    }

    $row_data[] = drupal_render($form['price'][$key]);

    if (isset($form['delete'][0])) {
      $row_data[] = drupal_render($form['delete'][$key]);
    }

    $rows[] = $row_data;
    $row_data = array();
  }

  if (in_array(t('Price'), $header)) {
    $row_data = array_fill(0, count($header) - 1, array());
    $row_data[count($header) - 1] = drupal_render($form['total']);
    $rows[] = $row_data;
  }

  $output .= '<div class="entries">'. theme('table', $header, $rows) . '</div>';

  $output .= drupal_render($form);

  return $output;
}

function build_entries_header($form, $item = 0) {
  $header = array();
  if (isset($form['qty'][$item])) {
    $header[] = t('Qty');
  }
  if (isset($form['item_desc'][$item])) {
    $header[] = t('Stock Code');
  }
  if (isset($form['completed_date'][$item])) {
    $header[] = t('Date');
  }
  if (isset($form['extra'][$item])) {
    $header[] = t('Extra Text');
  }
  if (isset($form['serials'][$item]) or isset($form['serial'][$item])) {
    $header[] = t('Serial');
  }
  if (isset($form['price'][$item])) {
    $header[] = t('Price');
  }
  if (isset($form['delete'][$item])) {
    $header[] = t('Delete');
  }
  return $header;
}

/**
 * Theme newline entries
 */
function theme_newline($form) {
  $rows = array();

  $header = build_entries_header($form, '#id');

  if (isset($form['qty']['#id'])) {
    $row_data[] = drupal_render($form['qty']);
  }
  if (isset($form['item_desc']['#id'])) {
    $row_data[] = drupal_render($form['item_desc']);
  }
  if (isset($form['completed_date']['#id'])) {
    $row_data[] = drupal_render($form['completed_date']);
  }
  if (isset($form['extra']['#id'])) {
    $row_data[] = drupal_render($form['extra']);
  }
  if (isset($form['serials']['#id'])) {
    $row_data[] = drupal_render($form['serials']);
  }
  if (isset($form['serial']['#id'])) {
    $row_data[] = drupal_render($form['serial']);
  }
  if (isset($form['price']['#id'])) {
    $row_data[] = drupal_render($form['price']);
  }

  $rows[] = $row_data;

  $output .= '<div class="newline">'. theme('table', $header, $rows) . '</div>';

  $output .= drupal_render($form);

  return $output;
}

function theme_erp_amount($amount) {
  return sprintf("$%-12.2f", $amount);
}

function erp_get_total($node) {
  $total = 0;
  if ($node->entries) {
    for ($i=0; $i < count($node->entries['qty']); $i++) {
      $total += $node->entries['qty'][$i] * $node->entries['price'][$i];
    }
  }
  return $total;
}

function erp_cmp_by_weight($a, $b) {
  $a = (isset($a['#weight']) ? $a['#weight'] : 0);
  $b = (isset($b['#weight']) ? $b['#weight'] : 0);
  return ($a < $b ? -1 : ($a == $b ? 0 : +1));
}

function erp_set_customer(&$node) {
  if (!$node->customer_id) {
    if ($_GET['customer']) {
      $node->customer_id = $_GET['customer'];
    }
    elseif ($_SESSION['curr_customer']) {
      $node->customer_id = $_SESSION['curr_customer'];
    }
  }

  if ($node->customer_id && !isset($node->customer)) {
    $customer = node_load($node->customer_id);
    $node->customer = $customer->title;
  }
}
