<?php

/**
 * @file
 * ERP core default views
 */
function erp_views_default_views() {

  $views[$view->name] = $view;

  return $views;
}
