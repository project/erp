<?php

/**
 * @file
 * Provide admin functions for erp core
 */

/**
 * Provide some basic overview information about the erp system.
 * Note, that using module_invoke_all might be preferable, but would then need some
 * sort of ordering system.
 *
 * TODO: This should  probably converted to a per user commission style statement.
 *
 * @return unknown
 */
function erp_overview() {
  drupal_set_title(t('ERP Overview'));

  $output = '<div class="erp-details">';

  if (module_exists('erp_customer') or module_exists('erp_contact') or module_exists('erp_job') or module_exists('erp_timesheet')) {
    $output .= '<div class="erp-customers">';
    $output .= '<h2>' . t('Customers') . '</h2>';

    if (module_exists('erp_customer')) {

      // Customers
      $customer_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_customer}")));

      $output .= '<div class="erp-customer-details">';
      $output .= 'Customer Count: ' . $customer_count;
      $output .= '</div>';
    }

    if (module_exists('erp_contact')) {
      $contact_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_contact}")));

      $output .= '<div class="erp-contact-details">';
      $output .= 'Contact Count: ' . $contact_count;
      $output .= '</div>';
    }

    // Jobs & Timesheets
    if (module_exists('erp_job')) {
      $job_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_job}")));

      $output .= '<div class="erp-job-details">';
      $output .= 'Job Count: ' . $job_count;
      $output .= '</div>';
    }

    if (module_exists('erp_timesheet')) {
      $timesheet_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_timesheet}")));

      $output .= '<div class="erp-timesheet-details">';
      $output .= 'Timesheet Count: ' . $timesheet_count;
      $output .= '</div>';
    }

    $output .= '</div>';
  }

  if (module_exists('erp_cash_sale') or module_exists('erp_invoice') or module_exists('erp_payment')) {

    $output .= '<div class="erp-income">';
    $output .= '<h2>' . t('Income') . '</h2>';

    // Income
    if (module_exists('erp_cash_sale')) {
      $cash_sale_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_cash_sale}")));

      $output .= '<div class="erp-cash-sale-details">';
      $output .= 'Cash Sale Count: ' . $cash_sale_count;
      $output .= '</div>';
    }

    // Get amount of outstanding invoices
    if (module_exists('erp_invoice')) {
      $invoice_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_invoice}")));
      $invoice_outstanding = array_pop(db_fetch_array(db_query("SELECT sum(total) FROM {erp_invoice} WHERE invoice_status <> 'C'")));

      $output .= '<div class="erp-invoice-details">';
      $output .= 'Invoice Count: ' . $invoice_count;
      $output .= '</div>';

      $output .= '<div class="erp-invoice-outstanding">';
      $output .= 'Invoice Outstanding: ' . erp_symbol() . erp_currency($invoice_outstanding);
      $output .= '</div>';
    }

    if (module_exists('erp_payment')) {
      $payment_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_payment}")));

      $output .= '<div class="erp-payment-details">';
      $output .= 'Payment Count: ' . $payment_count;
      $output .= '</div>';
    }
    $output .= '</div>';
  }

  if (module_exists('erp_purchase_order') or module_exists('erp_goods_receive')) {
    $output .= '<div class="erp-outgoings">';
    $output .= '<h2>' . t('Outgoings') . '</h2>';

    // Outgoings
    if (module_exists('erp_purchase_order')) {
      $purchase_order_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_purchase_order}")));

      $output .= '<div class="erp-purchase-order-details">';
      $output .= 'Purchase Order Count: ' . $purchase_order_count;
      $output .= '</div>';
    }

    if (module_exists('erp_goods_receive')) {
      $goods_receive_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_goods_receive}")));

      $output .= '<div class="erp-goods-receive-details">';
      $output .= 'Goods Receipt Count: ' . $goods_receive_count;
      $output .= '</div>';
    }
    $output .= '</div>';
  }

  if (module_exists('erp_item') or module_exists('erp_stock')) {
    $output .= '<div class="erp-items-stock">';
    $output .= '<h2>' . t('Items and Stock') . '</h2>';

    // Items & Stock
    if (module_exists('erp_item')) {
      $item_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_item}")));

      $output .= '<div class="erp-item-details">';
      $output .= 'Item Count: ' . $item_count;
      $output .= '</div>';
    }

    if (module_exists('erp_stock')) {
      $stock_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_stock}")));

      $output .= '<div class="erp-goods-receive-details">';
      $output .= 'Stock Count: ' . $stock_count;
      $output .= '</div>';
    }
    $output .= '</div>';
  }

  if (module_exists('erp_store')) {
    $output .= '<div class="erp-other">';
    $output .= '<h2>' . t('Other info') . '</h2>';

    // Stores & Users
    if (module_exists('erp_store')) {
      $store_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {erp_store}")));
      $user_count = array_pop(db_fetch_array(db_query("SELECT COUNT(*) FROM {users}")));

      $output .= '<div class="erp-store-details">';
      $output .= 'Store Count: ' . $store_count;
      $output .= '</div>';

      $output .= '<div class="erp-user-details">';
      $output .= 'User Count: ' . $user_count;
      $output .= '</div>';
    }

    $output .= '</div>';
  }


  $output .= '</div>';

  return $output;
}

function erp_initial_setup_check() {
  $erp_initial_setup_complete = variable_get('erp_initial_setup_complete', FALSE);
  if (!$erp_initial_setup_complete) {
    return t('Your ERP setup is not yet complete, please visit !url to finalise settings', array('!url' => l(t('ERP Initial setup'), 'admin/erp/initialsetup')));
  }
}

/**
 * This function is to make things work better with auto deployment tools like aegir
 * that dont allow any input at profile runthrough time.
 *
 * It just incorporates all the admin setting forms into one place.
 *
 * TODO: Should be moved from this module, not needed for every page.
 *
 */
function erp_initial_setup() {
  $form = array();

  $erp_initial_setup_complete = variable_get('erp_initial_setup_complete', FALSE);
  if (!$erp_initial_setup_complete) {
    $form['help'] = array(
      '#type' => 'markup',
      '#value' => t('<p>ERP is now setup, but there are some tasks that should be reviewed before you start using ERP.</p><p>The tasks are listed below, with links to various things that should be personalised for your business.</p><p>Please work through the list below, and mark each item off as you complete it.</p>'),
    );

    $form['help']['#prefix'] = '<div class="error">';
    $form['help']['#suffix'] = '</div><p>&nbsp;</p>';
  }
  else {
    $form['help'] = array(
      '#type' => 'markup',
      '#value' => t('<p>Congratulations!</p><p>ERP setup is now complete.</p>'),
    );
    $form['help']['#suffix'] = '<p>&nbsp;</p>';
  }

  drupal_set_title(t('ERP Setup tasks'));
  $form['setup_tasks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Setup tasks'),
    '#collapsible' => TRUE,
    '#collapsed' => $erp_initial_setup_complete
  );

  $store = FALSE;
  $default_store = variable_get('erp_default_store', FALSE);
  if ($default_store) {
    $store = node_load(erp_id_to_nid('store', $default_store));
  }
  if (!$store) {
    $store_count = db_query("SELECT COUNT(nid) FROM {erp_store}")->fetchField();
    
    $form['setup_tasks']['erp_setup_store_missing'] = array(
      '#type' => 'item',
      '#markup' => l(t('Add a store?'), 'node/add/erp-store', array('query' => array('destination' => $_GET['q']))),
    );
    
    if ($store_count < 1) {
      $form['setup_tasks']['erp_setup_store_missing']['#description'] = t('There are no existing stores, you may need to add a new one');
    }
    else {
      $form['setup_tasks']['erp_setup_store_missing']['#description'] = t('There is !count !stores already, perhaps you just need to select a default', array('!count' => $store_count, '!stores' => format_plural($store_count, t('store'), t('stores'))));
    }    
    
    $form['setup_tasks']['erp_setup_store_default'] = array(
      '#type' => 'item',
      '#markup' => l(t('Select default'), 'admin/erp/store', array('query' => array('destination' => $_GET['q']))),
      '#description' => t('There is no default store selected'),
    );    
  }
  else {
    $form['setup_tasks']['erp_setup_store_complete'] = array(
      '#type' => 'checkbox',
      '#title' => t('I have edited the store details.'),
      '#default_value' => variable_get('erp_setup_store_complete', FALSE),
      '#description' => t('!url - Ensure that the store name and address match your business.', array('!url' => l($store->title, 'node/' . $store->nid . '/edit', array('query' => array('destination' => $_GET['q']))))),
    );
  }

  $form['setup_tasks']['erp_setup_add_users'] = array(
   '#type' => 'checkbox',
    '#title' => t('I have added users for my staff.'),
    '#default_value' => variable_get('erp_setup_add_users', FALSE),
    '#description' => t('!url - Add new users for your staff. Remember to select the "Staff" role for each that you add.', array('!url' => l(t('Drupal user list'), 'admin/people', array('query' => array('destination' => $_GET['q']))))),
  );

  $form['setup_tasks']['erp_setup_default_franchisee'] = array(
   '#type' => 'checkbox',
    '#title' => t('I have verified the default franchisee.'),
    '#default_value' => variable_get('erp_setup_default_franchisee', FALSE),
    '#description' => t('!url - Review the franchisee settings.', array('!url' => l(t('ERP franchisee settings'), 'admin/erp/franchise', array('query' => array('destination' => $_GET['q']))))),
  );

  $form['setup_tasks']['erp_setup_customer_defaults'] = array(
    '#type' => 'checkbox',
    '#title' => t('I have verified the customer defaults.'),
    '#default_value' => variable_get('erp_setup_customer_defaults', FALSE),
    '#description' => t('!url - Review the customer settings.', array('!url' => l(t('ERP customer settings'), 'admin/erp/customer', array('query' => array('destination' => $_GET['q']))))),
  );

  $form['setup_tasks']['erp_setup_invoice_defaults'] = array(
    '#type' => 'checkbox',
    '#title' => t('I have verified the invoice defaults.'),
    '#default_value' => variable_get('erp_setup_invoice_defaults', FALSE),
    '#description' => t('!url - Review the invoice settings.', array('!url' => l(t('ERP invoice settings'), 'admin/erp/invoice', array('query' => array('destination' => $_GET['q']))))),
  );

  $form['setup_tasks']['erp_setup_gmap_defaults'] = array(
    '#type' => 'checkbox',
    '#title' => t('I have verified the GMap (Google Map) defaults.'),
    '#default_value' => variable_get('erp_setup_gmap_defaults', FALSE),
    '#description' => t('!url - Review the invoice settings.', array('!url' => l(t('GMap settings'), 'admin/config/gmap', array('query' => array('destination' => $_GET['q']))))),
  );

  $form['#submit'][] = 'erp_initial_setup_update';

  return system_settings_form($form);
}

function erp_initial_setup_update($form, $form_state) {
  $complete = TRUE;
  foreach ($form_state['values'] as $var => $value) {
    if (substr($var, 0, 9) == 'erp_setup') {
      if ($value != 1) {
        $complete = FALSE;
        break;
      }
    }
  }
  if ($complete) {
    variable_set('erp_initial_setup_complete', TRUE);
  }
  else {
    variable_set('erp_initial_setup_complete', FALSE);
  }
}
