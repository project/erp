<div class="store-info">
  <div class="erp-store-logo">
    <?php
      $erp_store_logo = _erp_store_fileobj($node, 1);
      //print theme('erp_store_logo', $erp_store_logo);
    ?>
  </div>
  
  <div class="store-id">
    Store id: <?php print $node['store_id']; ?>
  </div>

  <div class="active">
    <?php
      if ($node['active']) {
        print t('Active');
      }
      else {
        print t('Inactive');
      }
    ?>
  </div>

  <div class="company">
    Company: <?php print $node['company']; ?>
  </div>

  <div class="tax-id">
  Tax id: <?php print $node['tax_id']; ?>
  </div>

  <div class="address">
  Address: <?php print $node['address']; ?>
  </div>

  <div class="suburb">
  Suburb: <?php print $node['suburb']; ?>
  </div>

  <div class="state">
  State: <?php print $node['state']; ?>
  </div>

  <div class="postcode">
  Postcode: <?php print $node['postcode']; ?>
  </div>

  <div class="phone">
  Phone: <?php print $node['phone']; ?>
  </div>

  <div class="fax">
  Fax: <?php print $node['fax']; ?>
  </div>

  <div class="mobile">
  Mobile: <?php print $node['mobile']; ?>
  </div>

  <div class="email">
  Email: <?php print $node['email']; ?>
  </div>

  <?php
    //print implode(module_invoke_all('erp_store_info', $node));
  ?>

  <div class="homepage">
  Homepage: <?php print l($node['homepage'], $node['homepage']); ?>
  </div>

  <div class="bank-details">
  Bank Details: <?php print $node['bank_details']; ?>
  </div>

  <div class="documentation">
    <?php   
    if (module_exists('book')) {
      if ($node['documentation']) {
        $book = node_load($node['documentation']);
        print t('Documentation: ') . l($book['title'], 'node/' . $book['nid']);
      }
    }
    ?>
  </div>
</div>
