<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function erp_profile_modules() {
  return array(
               // Core drupal modules
               'block', 'comment', 'filter', 'help', 'menu', 'node', 'system', 'taxonomy', 'user', 'watchdog',

               // Other core modules we use
               'book', 'search', 'upload',

               // Other contrib modules we use
               'views', 'views_ui', 'views_rss', 'jstools', 'jscalendar', 'tabs', 'event',
               'workflow',

               // Now the erp modules - core
               'erp', 'erp_cart', 'erp_store',
               
               // Accounting/sales
               'erp_accounting', 'erp_tax', 'erp_stock', 'erp_quote', 'erp_cash_sale', 
               'erp_invoice', 'erp_purchase_order', 'erp_item', 
               'erp_goods_receive', 'erp_payment', 'erp_supplier', 'erp_timesheet',
               'erp_recurring', 'erp_pricing',

               // Customer
               'erp_contact', 'erp_customer', 

               // Job modules
               'erp_job', 'erp_job_billable',

               // Auxillary utilities
               'erp_email', 'erp_formfixes', 'erp_franchise', 'erp_google_maps', 
               'erp_print', 'pdf'

               // Referrals/Redemptions - not ready for 5.x yet
               //'erp_referral', 'erp_redemption',

               // Price list and more
               //'erp_price_list', 'erp_reports', 'erp_warranty'

               // SMS utilities
               //'erp_sms', 'erp_sms_job_notify', 'erp_sms_message'
               );
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function erp_profile_details() {
  return array(
    'name' => 'erp',
    'description' => 'This installation profile will setup a basic ERP system.'
  );
}

/**
 * Perform any final installation tasks for this profile.
 *
 * @return
 *   An optional HTML string to display to the user on the final installation
 *   screen.
 */
function erp_profile_final() {
  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;

  variable_set('theme_settings', $theme_settings);

  // Make an administrator role
  db_query("INSERT INTO {role} (rid, name) VALUES (3, 'administrator')");
  // Add user 1 to the 'administrator' role
  db_query("INSERT INTO {users_roles} VALUES (1, 3)");
  // Set permissions
  db_query("INSERT INTO `permission` VALUES (3,'create events, edit own events, administer blocks, create book pages, create new books, edit book pages, edit own book pages, outline posts in books, see printer-friendly version, access comments, administer comments, post comments, post comments without approval, access erp, admin erp, erp quick find block, admin erp accounting, view erp accounting, access erp cart, admin erp cart, add cash_sale, admin cash_sale, edit cash_sale, edit own cash_sale, erp contact edit, erp contact list, erp contact remove, erp contact save, admin customers, autocomplete customers, create customer, customer quick find block, edit all customers, edit customer, edit own customer, list customers, admin erp_email, send email, admin franchise, add goods_receipt, admin goods_receive, edit goods_receipt, edit own goods_receipt, admin google_maps, view erp google map, add invoice, admin invoice, edit invoice, edit own invoice, view statements, admin item, item cart, item edit, item list, item price list, item wholesale, add job, admin job, edit all jobs, edit job, edit own job, job list, open jobs, admin payment, edit payment, list payments, receive payment, admin pricing, view pricing levels, erp print, add purchase_order, admin purchase_order, edit own purchase_order, edit purchase_order, add quote, admin quote, edit own quote, edit quote, admin stock, stock adjust, stock cart, stock edit, stock list, stock wholesale, admin store, create store, edit all stores, edit own store, edit store, store list, admin supplier, create supplier, edit supplier, supplier autocomplete, supplier list, access erp tax, admin erp tax, create timesheet, edit timesheet, list timesheets, administer filters, administer menu, access content, administer content types, administer nodes, revert revisions, view revisions, administer organic groups, create pdf, administer search, search content, use advanced search, access administration pages, administer site configuration, select different theme, administer taxonomy, upload files, view uploaded files, access user profiles, administer access control, administer users, change own username, access all views, administer views, administer workflow, schedule workflow transitions',0)");

  // Make a staff role
  db_query("INSERT INTO {role} (rid, name) VALUES (4, 'staff')");
  // Add user 1 to the 'admin user' role
  db_query("INSERT INTO {users_roles} VALUES (1, 4)");
  // Set permissions
  db_query("INSERT INTO `permission` VALUES (4,'create events, edit own events, create book pages, edit book pages, edit own book pages, see printer-friendly version, access comments, post comments, post comments without approval, access erp, erp quick find block, view erp accounting, access erp cart, add cash_sale, edit cash_sale, edit own cash_sale, erp contact edit, erp contact list, erp contact remove, erp contact save, autocomplete customers, create customer, customer quick find block, edit all customers, edit customer, edit own customer, list customers, send email, add goods_receipt, edit goods_receipt, edit own goods_receipt, view erp google map, add invoice, edit invoice, edit own invoice, view statements, item cart, item edit, item list, item price list, item wholesale, add job, edit all jobs, edit job, edit own job, job list, open jobs, edit payment, list payments, receive payment, view pricing levels, erp print, add purchase_order, edit own purchase_order, edit purchase_order, add quote, edit own quote, edit quote, stock adjust, stock cart, stock edit, stock list, stock wholesale, create store, edit all stores, edit own store, edit store, store list, create supplier, edit supplier, supplier autocomplete, supplier list, access erp tax, create timesheet, edit timesheet, list timesheets, access content, create pdf, search content, use advanced search, upload files',0)");

  // Create a store and make it the default
  $store->title = 'Default';
  $store->type = 'erp_store';
  $store->company = 'Default company';
  $store->status = 1;
  node_save($store);
  variable_set('erp_default_store', $store->sid);

  // Set some drupal defaults
  variable_set('site_frontpage', 'job');
  variable_set('site_name', 'drupal erp');

  // Enable erp theme
  //db_query("UPDATE {system} SET status = 1 WHERE name = 'erp_theme'");
  //variable_set('theme_default', 'erp_theme');

  // Turn off all "promoting", drupal things first
  $types = array('book', 'event', 

                 // Now erp things
                 'erp_cash_sale', 'erp_customer', 'erp_goods_receive', 'erp_invoice', 'erp_item', 
                 'erp_job', 'erp_payment', 'erp_purchase_order', 'erp_quote', 'erp_store', 
                 'erp_supplier', 'erp_timesheet');

  foreach ($types as $type) {
    variable_set('node_options_'. $type, array('status'));
  }

  // Add erp block
  db_query("INSERT INTO {blocks} VALUES ('block', '1', 'garland', 1, -10, 'left', 0, 0, 0, '', 'Links')");
  db_query('INSERT INTO {boxes} VALUES(1, \'
<?php
global $user;
global $base_url;
?>
<div class="menu">
<ul>
<li class="leaf"><a href="<?php echo $base_url?>">Home</a></li>
<?php
if (!$user->uid):
  echo \"<li class=\\\"leaf\\\"><a href=\\\"$base_url/user\\\">Log in</a></li>\";
else: ?>
<li class="leaf"><a href="<?php echo $base_url?>/book">Documentation</a></li>
<li class="leaf"><a href="<?php echo $base_url?>/event">Coming Events</a></li>
<li class="leaf"><a href="<?php echo $base_url?>/logout">Log out</a></li>
<?php
endif;
?>
</ul>
</div>\', \'Links\', 2)');

  // Hide the user login block, the above does the same job
  db_query("UPDATE {blocks} SET status = 0 WHERE module = 'user' AND delta = 0");

  // Setup erp customer search
  db_query("UPDATE {blocks} SET status = 1, region = 'left', weight = 1, theme = 'garland' WHERE module = 'erp'");
  db_query("UPDATE {blocks} SET status = 1, region = 'left', weight = 2, theme = 'garland' WHERE module = 'erp_customer'");
  db_query("UPDATE {blocks} SET status = 1, region = 'left', weight = 3, theme = 'garland' WHERE module = 'erp_cart'");

  // Put admin block on right
  db_query("UPDATE {blocks} SET region = 'right' WHERE module = 'user' AND delta = 1");

  // Hide some standard menus we dont want to see. (Doesn't seem to work).
  db_query("UPDATE {menu} SET type = 48 WHERE path = 'og' AND title = 'Groups'");
  db_query("UPDATE {menu} SET type = 48 WHERE path = 'tracker' AND title = 'Recent posts'");

  // The return message is optional, if you omit it the default will be used.
  $errors = drupal_set_message();
  
  if ($errors) {
    return t('Please review the messages above before continuing on to create an admin account as part of your drupal erp setup. <a href="@url">your new drupal erp site</a>.', array('@url' => url('user/register')));
  }
  else {
    return t('You may now create an admin account as part of your drupal erp setup. <a href="@url">your new Profile Name site</a>.', array('@url' => url('user/register')));
  }

  cache_clean_all();
  menu_rebuild();
  _block_rehash();
}
